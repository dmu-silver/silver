class AddFileUrlToProductVideo < ActiveRecord::Migration
  def change
    add_column :product_videos, :file_url, :string
  end
end
