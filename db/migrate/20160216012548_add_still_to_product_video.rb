class AddStillToProductVideo < ActiveRecord::Migration
  def change
    add_column :product_videos, :still, :string
  end
end
