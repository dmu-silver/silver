class CreateProductVideos < ActiveRecord::Migration
  def change
    create_table :product_videos do |t|
      t.string :url
      t.integer :product_id
      t.timestamps null: false
    end
  end
end
