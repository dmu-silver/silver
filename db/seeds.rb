# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#AdminUser.create!(email: 'p14157409@myemail.dmu.ac.uk', password: 'password', password_confirmation: 'password')
#AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

Category.create(:id => 1, :name => "Phone")
Category.create(:id => 2, :name => "Network")
Category.create(:id => 3, :name => "Upgrade")
Category.create(:id => 4, :name => "Asessories")

Brand.create(:id => 1, :name => "Apple ", :image => 'Phone1.jpg', :description => "Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.")
Brand.create(:id => 2, :name => "Samsung ", :image => 'IMAGE2.jpg', :description => "Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.")
Brand.create(:id => 3, :name => "LG ", :image => 'IMAGE3.jpg', :description => "Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.")

Product.create(:id => 1, :name => 'Phone1', :price => 522.52, :description => '3D Touch, Live Photos, 7000 series aluminium, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more', :category_id => 1, :brand_id => 1, :stock => 5, :highlighted => true)
Product.create(:id => 2, :name => 'Phone2', :price => 485.66, :description => '3D Touch, Live Photos, 7000 series aluminium, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more', :category_id => 1, :brand_id => 1, :stock => 10, :highlighted => true)
Product.create(:id => 3, :name => 'Accessory 1', :price => 325.45, :description => '3D Touch, Live Photos, 7000 series aluminium, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more', :category_id => 4, :brand_id => 2, :stock => 3)
Product.create(:id => 4, :name => 'Accessory 2', :price => 774.55, :description => '3D Touch, Live Photos, 7000 series aluminium, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more', :category_id => 4, :brand_id => 3, :stock => 8)
Product.create(:id => 5, :name => 'Accessory 3', :price => 774.55, :description => '3D Touch, Live Photos, 7000 series aluminium, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more', :category_id => 4, :brand_id => 3, :stock => 0)

Product.find(1).associated_products << Product.find(3)
Product.find(1).associated_products << Product.find(4)

Product.find(2).associated_products << Product.find(3)
Product.find(2).associated_products << Product.find(4)
Product.find(2).associated_products << Product.find(5)

ProductImage.create(:product_id => 1, :url => File.open(File.join(Rails.root, 'app/assets/images/Phone1.jpg')))
ProductImage.create(:product_id => 2, :url => File.open(File.join(Rails.root, 'app/assets/images/Phone2.jpg')))
ProductImage.create(:product_id => 3, :url => File.open(File.join(Rails.root, 'app/assets/images/Phone3.jpg')))
ProductImage.create(:product_id => 4, :url => File.open(File.join(Rails.root, 'app/assets/images/Phone4.jpg')))
ProductImage.create(:product_id => 5, :url => File.open(File.join(Rails.root, 'app/assets/images/Phone5.jpg')))
ProductImage.create(:product_id => 1, :url => File.open(File.join(Rails.root, 'app/assets/images/Phone6.jpg')))
ProductImage.create(:product_id => 2, :url => File.open(File.join(Rails.root, 'app/assets/images/Phone1.jpg')))
ProductImage.create(:product_id => 3, :url => File.open(File.join(Rails.root, 'app/assets/images/Phone2.jpg')))

ProductVideo.create(:product_id => 1, :file_url => "https://youtu.be/eI8YZdejPKg", :still => File.open(File.join(Rails.root, 'app/assets/images/IMAGE1.jpg')))
ProductVideo.create(:product_id => 2, :file_url => File.open(File.join(Rails.root, 'app/assets/videos/VIDEO2.mp4')), :still => File.open(File.join(Rails.root, 'app/assets/images/IMAGE2.jpg')))
ProductVideo.create(:product_id => 3, :file_url => File.open(File.join(Rails.root, 'app/assets/videos/VIDEO3.mp4')), :still => File.open(File.join(Rails.root, 'app/assets/images/IMAGE3.jpg')))
ProductVideo.create(:product_id => 4, :file_url => File.open(File.join(Rails.root, 'app/assets/videos/VIDEO4.mp4')), :still => File.open(File.join(Rails.root, 'app/assets/images/IMAGE4.jpg')))

