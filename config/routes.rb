Rails.application.routes.draw do
  
  post '/rate' => 'rater#create', :as => 'rate'
  get 'carty/show'

  root 'page#index'
  get '/about', to: 'page#about'
  match '/contact', to: 'contacts#new', via: 'get'
  resources "contacts", only: [:new, :create]
  
  resources :users, except: [:index]
  
  resources :reviews

  post  'reviews/new/:id', to: 'reviews#new'
  get   'reviews/new/:id', to: 'reviews#new'
  post  'reviews/displays/:id', to: 'reviews#displays'
  get   'reviews/displays/:id', to: 'reviews#displays'

  resources :carts
  get 'errors/routing'

  
  controller :sessions do
    get  'login' => :new        # login_path
    post 'login' => :create
    get 'logout' => :destroy    # logout_path
  end

  
  
  get 'products/search', to: 'products#search'
  post 'products/search', to: 'products#search'

  

  resources :categories
  resources :products do
    collection do
      get :search
    end
  end
  resources :brands
  resources :carts
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  match '*a', to: 'errors#routing',   via: [:get, :post]
    
end
