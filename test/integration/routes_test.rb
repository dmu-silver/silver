require 'test_helper'

class RoutesTest < ActionDispatch::IntegrationTest
  test "should route to product" do
    assert_routing '/products/1', { controller: "products", action: "show", id: "1" }
  end
  
  test "should route to products" do
    assert_routing '/products', { controller: "products", action: "index" }
  end
  
  test "should route to about" do
    assert_routing '/about', { controller: "page", action: "about" }
  end
  
  test "should route to index" do
    assert_routing '/', { controller: "page", action: "index" }
  end
  
  test "should route to contact" do
    assert_routing '/contact', { controller: "contacts", action: "new" }
  end
  
  test "should route to new user" do
    assert_routing '/users/new', { controller: "users", action: "new" }
  end
  
  test "should route to create user" do
    assert_routing({ method: 'post', path: '/products' }, { controller: "products", action: "create" })
  end
  
  test "should route to user" do
    assert_routing '/users/1', { controller: "users", action: "show", id: "1" }
  end
  
  test "should route to edit user" do
    assert_routing '/users/1/edit', { controller: "users", action: "edit", id: "1" }
  end
  
  test "should route to update user" do
    assert_routing({ method: 'put', path: '/products/1' }, { controller: "products", action: "update", id: "1" })
  end
end