require 'test_helper'

class ProductVideosControllerTest < ActionController::TestCase
  setup do
    @product_video = product_videos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_videos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_video" do
    assert_difference('ProductVideo.count') do
      post :create, product_video: {  }
    end

    assert_redirected_to product_video_path(assigns(:product_video))
  end

  test "should show product_video" do
    get :show, id: @product_video
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_video
    assert_response :success
  end

  test "should update product_video" do
    patch :update, id: @product_video, product_video: {  }
    assert_redirected_to product_video_path(assigns(:product_video))
  end

  test "should destroy product_video" do
    assert_difference('ProductVideo.count', -1) do
      delete :destroy, id: @product_video
    end

    assert_redirected_to product_videos_path
  end
end
