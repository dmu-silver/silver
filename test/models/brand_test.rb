require 'test_helper'

class BrandTest < ActiveSupport::TestCase
  
  test "should not save brand with no name" do
    brand = Brand.new(:description => "descrition")
    
    assert_not brand.save, "Saved brand without a name"
  end
  
  test "should not save brand with no description" do
    brand = Brand.new(:name => "Brand1")
    
    assert_not brand.save, "Saved brand without a description"
  end
  
  test "should not save brand with wrong length of name" do
    brand = Brand.new(:name => "B", :description => "description")
    assert_not brand.save, "Saved brand with short name"
    
    brand = Brand.new(:name => "Brand with a name too long to pass the assert", :description => "description")
    assert_not brand.save, "Saved brand with long name"
  end
  
  test "should not save brand with long description" do
    brand = Brand.new(:name => "Brand", :description => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at tortor ac mauris posuere mollis id vel dui. Nulla vitae risus quam. Nunc at convallis nibh, id malesuada sapien. Donec orci arcu, pellentesque eu ante eu, ultrices venenatis quam. Maecenas vel lorem at felis consectetur lacinia. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean quis eros a nisl sollicitudin porta a eget quam. Mauris augue nisl, malesuada vitae erat eu, vulputate blandit augue. Ut tempus sed sapien non fringilla. Proin placerat tortor sed felis iaculis, ut lobortis nisl mattis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent eget turpis sodales, ornare nisl in, commodo libero. Aenean id mauris at urna faucibus efficitur. Nunc laoreet, massa id aliquet finibus, metus nunc hendrerit mauris, quis laoreet magna urna sed augue. Integer non augue euismod, malesuada mi non, viverra massa. Aliquam dui massa, posuere vitae molestie at, congue id tortor. Aliquam at tortor ac mauris posuere mollis id vel dui. Nulla vitae risus quam. Nunc at convallis nibh, id malesuada sapien. Donec orci arcu, pellentesque eu ante eu, ultrices venenatis quam. Maecenas vel lorem at felis consectetur lacinia. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean quis eros a nisl sollicitudin porta a eget quam. Mauris augue nisl, malesuada vitae erat eu, vulputate blandit augue. Ut tempus sed sapien non fringilla. Proin placerat tortor sed felis iaculis, ut lobortis nisl mattis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent eget turpis sodales, ornare nisl in, commodo libero. Aenean id mauris at urna faucibus efficitur. Nunc laoreet, massa id aliquet finibus, metus nunc hendrerit mauris, quis laoreet magna urna sed augue. Integer non augue euismod, malesuada mi non, viverra massa. Aliquam dui massa, posuere vitae molestie at, congue id tortor.")
    assert_not brand.save, "Saved brand with long description"
    
  end
  
end
