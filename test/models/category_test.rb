require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  test "should not save category with no name" do
    category = Category.new
    
    assert_not category.save, "Saved category without a name"
  end
  
  test "should not save category with wrong length of name" do
    category = Category.new(:name => "C")
    assert_not category.save, "Saved category with short name"
    
    category = Category.new(:name => "Category with a name too long to pass the assert")
    assert_not category.save, "Saved category with long name"
  end
  
  
end
