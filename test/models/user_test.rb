require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  test "should not save user with no name" do
    user = User.new(:email => "email@example.com", :password_digest => "password")
    
    assert_not user.save, "Saved user without a name"
  end
  
  test "should not save user with same name" do
    user = User.new(:name => "User", :email => "email@example.com", :password_digest => "password")
    user.save
    
    user2 = User.new(:name => "User", :email => "email2@example.com", :password_digest => "password")
    assert_not user2.save, "Saved user with same name"
  end
  
  test "should not save user with no email" do
    user = User.new(:name => "User", :password_digest => "password")
    
    assert_not user.save, "Saved user without an email"
  end
  
  test "should not save user with same email" do
    user = User.new(:name => "User", :email => "email@example.com", :password_digest => "password")
    user.save
    
    user2 = User.new(:name => "User2", :email => "email@example.com", :password_digest => "password")
    assert_not user2.save, "Saved user with same name"
  end
  
end
