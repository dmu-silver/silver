require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  test "should not save product with no name" do
    product = Product.new(:description => "descrition", :price => 1, :category_id => 1)
    
    assert_not product.save, "Saved product without a name"
  end
  
  test "should not save product with no description" do
    product = Product.new(:name => "Product", :price => 1, :category_id => 1)
    
    assert_not product.save, "Saved product without a description"
  end
  
  test "should not save product with wrong length of name" do
    product = Product.new(:name => "P", :description => "description", :price => 1, :category_id => 1)
    assert_not product.save, "Saved product with short name"
    
    product = Product.new(:name => "Product with a name too long to pass the assert", :description => "description", :price => 1, :category_id => 1)
    assert_not product.save, "Saved product with long name"
  end
  
  test "should not save product with long description" do
    product = Product.new(:name => "Product", :price => 1, :category_id => 1, :description => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at tortor ac mauris posuere mollis id vel dui. Nulla vitae risus quam. Nunc at convallis nibh, id malesuada sapien. Donec orci arcu, pellentesque eu ante eu, ultrices venenatis quam. Maecenas vel lorem at felis consectetur lacinia. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean quis eros a nisl sollicitudin porta a eget quam. Mauris augue nisl, malesuada vitae erat eu, vulputate blandit augue. Ut tempus sed sapien non fringilla. Proin placerat tortor sed felis iaculis, ut lobortis nisl mattis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent eget turpis sodales, ornare nisl in, commodo libero. Aenean id mauris at urna faucibus efficitur. Nunc laoreet, massa id aliquet finibus, metus nunc hendrerit mauris, quis laoreet magna urna sed augue. Integer non augue euismod, malesuada mi non, viverra massa. Aliquam dui massa, posuere vitae molestie at, congue id tortor. Aliquam at tortor ac mauris posuere mollis id vel dui. Nulla vitae risus quam. Nunc at convallis nibh, id malesuada sapien. Donec orci arcu, pellentesque eu ante eu, ultrices venenatis quam. Maecenas vel lorem at felis consectetur lacinia. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean quis eros a nisl sollicitudin porta a eget quam. Mauris augue nisl, malesuada vitae erat eu, vulputate blandit augue. Ut tempus sed sapien non fringilla. Proin placerat tortor sed felis iaculis, ut lobortis nisl mattis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent eget turpis sodales, ornare nisl in, commodo libero. Aenean id mauris at urna faucibus efficitur. Nunc laoreet, massa id aliquet finibus, metus nunc hendrerit mauris, quis laoreet magna urna sed augue. Integer non augue euismod, malesuada mi non, viverra massa. Aliquam dui massa, posuere vitae molestie at, congue id tortor.")
    assert_not product.save, "Saved product with long description"
  end
  
  test "should not save product with no category" do
    product = Product.new(:name => "Product", :description => "description", :price => 1)
    
    assert_not product.save, "Saved product without a category"
  end
  
  test "should not save product with no price" do
    product = Product.new(:name => "Product", :description => "description", :category_id => 1)
    
    assert_not product.save, "Saved product without a price"
  end
  
  test "should not save product with wrong price" do
    product = Product.new(:name => "Product", :description => "description", :category_id => 1, :price => -1)
    assert_not product.save, "Saved product without negative price"
    
    product = Product.new(:name => "Product", :description => "description", :category_id => 1, :price => "price")
    assert_not product.save, "Saved product without string price"  
  end
  
end
