ActiveAdmin.register Brand do

  permit_params :name, :image, :description
  
  index do
    column :image
    column :name
    column :description
    actions
  end
  
  filter :name
  
  show do
    attributes_table do
      row :name
      row :image
      row :description
    end
  end
  
  form do |f|  
    f.semantic_errors
    
    f.inputs do
      f.input :name
      f.input :image, :as => :file
      f.input :description
    end
    
    f.actions
  end


end
