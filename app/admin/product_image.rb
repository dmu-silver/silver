ActiveAdmin.register ProductImage do
  belongs_to :product
  
  permit_params :url, :product_id
  
end
