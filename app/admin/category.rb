ActiveAdmin.register Category do
  
  permit_params :name
  
  index do
    column :name
    actions
  end
  
  filter :name
  
  show do
    attributes_table do
      row :name
    end
  end
  
  form do |f|  
    f.semantic_errors
    
    f.inputs do
      f.input :name
    end
    
    f.actions
  end


end
