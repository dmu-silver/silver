ActiveAdmin.register Product do

  
  permit_params :category_id, :name, :price, :description, :highlighted, :brand_id, :associated_products,
    images_attributes: [ :id, :url, :product_id, :_destroy ],
    videos_attributes: [ :id, :url, :file_url, :still, :product_id, :_destroy ],
    associated_products_attributes: [:product_a_id, :product_b_id],
    product_a_ids: [],
    product_b_ids: []
  
  index do
    column :name
    column :price
    column :description
    column :highlighted
    column :category
    column :brand
    actions
  end
  
  filter :name
  filter :category
  filter :brand
  filter :price
  filter :description
  filter :highlighted
  
  show do
    attributes_table do
      row :category
      row :name
      row :price
      row :description
      row :highlighted
      row :brand
    end
    panel "Images" do
      table_for product.images do
        column "", :url do |i|
          image_tag i.url
        end
      end
    end
    
    panel "Videos" do
      table_for product.videos do
        column :still do |v|
          image_tag v.still
        end
      end
    end
  end
  
  form do |f|  
    f.semantic_errors
    
    f.inputs do
      f.input :category
      f.input :brand
      f.input :name
      f.input :price
      f.input :description
      f.input :highlighted
    end
    
    #f.inputs "Products", :multipart => true do 
      
    #  f.has_many :associated_products, allow_destroy: true, new_record: 'Add a product' do |b|
    #    b.input :associated_products, :as => :select, :input_html => { :multiple => false }, :collection => Product.all.map{ |p| ["#{p.name}", p.id]}
    #  end
    #end
    
    f.inputs "Images", :multipart => true do 
      f.has_many :images, allow_destroy: true, new_record: 'Add a image' do |b|
        b.input :url, :label => "File"
      end
    end
    
    f.inputs "Videos" , :multipart => true do
      f.has_many :videos, allow_destroy: true, new_record: 'Add a video' do |b|
        b.input :still, :label => "Video still"
        b.input :file_url, :label => "File"
        b.input :url, :label => "Url"
      end
    end
    f.actions
  end

end
