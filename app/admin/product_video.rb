ActiveAdmin.register ProductVideo do
  belongs_to :product
  
  permit_params :url, :file_url, :still, :product_id

end
