class UsersController < InheritedResources::Base

  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  
  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      redirect_to root_path, notice: "#{@user.name}: you have successfully registered."
    else
      render action: 'new'
    end
  end


  def update
    if @user.update(user_params)
      redirect_to users_path, notice: "#{@user.name}'s details were successfully updated."
    else
      render action: 'edit'
    end
  end

  def destroy
    session[:user_id] = nil
    @user.destroy
    
    redirect_to root_path
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :password, :password_confirmation, :email)
    end

end

