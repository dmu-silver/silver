class PageController < ApplicationController
  
  def index
     @products = Product.featured
  end
  
  def about
  end

  def contact
  end
end
