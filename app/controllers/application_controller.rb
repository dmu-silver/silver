class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :get_user
  helper_method :logged_in?, :user_name, :current_cart, :active_cart_colour, :record_history, :the_date
  
  protected   # these methods below can only be called by an object 
             # of this class and its subclasses  

  def the_date
    Time.now.strftime("%A #{Time.now.day.ordinalize} %B")
end

  def logged_in?    
    not session[:user_id].nil?  
  end 
  
  def user_name
    if session[:user_id] != nil
      user = User.find(session[:user_id])
      user.name
    end
  end

  # from an error generated when a request is 
  # made for a record with an 'id' that does not exist  
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found    

  private

  def get_user
    if session[:user_id] != nil
      @user = User.find(session[:user_id])
    end
  end

  def record_not_found    
    flash[:alert] = "Cannot find record number #{params[:id]}. Displaying all records."
    redirect_to root_path   
  end

  def current_cart
    Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
    cart = Cart.create
    session[:cart_id] = cart.id
    cart
  end

  def active_cart_colour
    if current_cart.empty?
      'cart_inactive'
    else
      'cart_active'
    end
  end

  
  def record_history
    session[:history] ||= []
    if session[:history].last != request.url 
      session[:history].push request.url
    end 
    session[:history] = session[:history].last(10) # limit the size to 10
  end
  
  
end
