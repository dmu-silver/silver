class ProductVideosController < InheritedResources::Base

  private

    def product_video_params
      params.require(:product_video).permit()
    end
end

