class ProductsController < InheritedResources::Base

  def index
      @products = Product.all.order(:name).page(params[:page]).per(6)
  end

  def show
    @product = Product.find(params[:id])
  end

  def search
    @search = Sunspot.search(Product) do 
      keywords params[:name]
      paginate :page => params[:page], :per_page => 1
    end
    
    @products = @search.results
    
    render :action => "index"
end

  private

    def product_params
      params.require(:product).permit()
    end
end

