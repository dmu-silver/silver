class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by email: params[:email]
    if user and user.authenticate params[:password]
      session[:user_id] = user.id
      redirect_to session[:return_to] || root_path
    else
      flash.now.alert = "Invalid user/password"
      render :new
    end
  end

  def destroy
    session[:user_id] = nil 
    if session[:cart_id]
      cart = Cart.find(session[:cart_id])
      cart.destroy
      session[:cart_id] = nil
    end
    redirect_to root_path 
  end

end
