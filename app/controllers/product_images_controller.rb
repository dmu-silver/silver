class ProductImagesController < InheritedResources::Base

  private

    def product_image_params
      params.require(:product_image).permit()
    end
end

