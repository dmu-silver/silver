json.array!(@product_videos) do |product_video|
  json.extract! product_video, :id
  json.url product_video_url(product_video, format: :json)
end
