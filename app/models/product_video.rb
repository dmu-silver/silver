class ProductVideo < ActiveRecord::Base
  mount_uploader :file_url, ProductVideoUploader
  mount_uploader :still, ProductVideoStillUploader
  
	belongs_to :product
end
