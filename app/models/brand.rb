class Brand < ActiveRecord::Base
  extend FriendlyId
  has_many :products

  friendly_id :name, use: [:slugged, :finders]
  
  validates :name, :presence => { :message => "cannot be blank ..."}, 
                   :length => {:in => 2..35, :message => "must be between two and thirty five characters"}
  
  validates :description, :presence => { :message => "cannot be blank ..."},
                          :length => {:maximum => 2000, :message => "maximum length 2000 characters"} 
  
end
