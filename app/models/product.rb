class Product < ActiveRecord::Base
	extend FriendlyId
	friendly_id :name, use: [:slugged, :finders]

	searchable do 
		text :name
		text :description
	end
	ratyrate_rateable 'rating'
	
	scope :featured, -> { where(highlighted: true) }
	
  
  belongs_to :category
	belongs_to :brand
	has_many :images, class_name: "ProductImage", :dependent => :destroy
	has_many :videos, class_name: "ProductVideo", :dependent => :destroy
	
	has_and_belongs_to_many(:associated_products,
	 :class_name => "Product",
   :join_table => "product_connections",
   :foreign_key => "product_a_id",
   :association_foreign_key => "product_b_id")
	
	has_many :cart_productss
	has_many :carts, through: :cart_products

	accepts_nested_attributes_for :images, :allow_destroy => true
  accepts_nested_attributes_for :videos, :allow_destroy => true
  accepts_nested_attributes_for :associated_products, :allow_destroy => true
  
  validates :category_id, :presence => { :message => "cannot be blank"}
	
	validates :name, :presence => { :message => "cannot be blank"},
	                 :length => {:in => 2..35, :message => "must be between two and thirty five characters"}
  
  validates :price, :presence => { :message => "cannot be blank"},
                    :numericality => {:greater_than_or_equal_to => 0, :allow_blank => false, :message => "must be a positive value"}

  validates :description, :presence => { :message => "cannot be blank"},
                          :length => {:maximum => 2000, :message => "maximum length 2000 characters"} 
  

  	def self.simple_search(search_string)
		self.where("name = ?", search_string)
	end

end
