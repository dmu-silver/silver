class ProductImage < ActiveRecord::Base
  mount_uploader :url, ProductImageUploader
  
	belongs_to :product
end
