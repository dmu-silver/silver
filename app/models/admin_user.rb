class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable
  
  before_destroy :check_if_last

  private

  def check_if_last
    if (AdminUser.all.count == 1)
      errors.add(:base, "Cannot delete last admin")
      return false
    end
  end
end
