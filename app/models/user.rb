class User < ActiveRecord::Base
  has_many :carts
  has_many :reviews
  
  validates :name, presence: true, uniqueness: true
  validates :email, uniqueness: true, email_format: { message: "That is not a valid email address." }
  
  ratyrate_rater 
  has_secure_password
end
